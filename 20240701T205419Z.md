In the Bible, it is recorded that people brought children to Jesus so that He might touch and bless them. This event is mentioned in several of the Gospels:

- In Matthew 19:13-15, it says: "Then people brought little children to Jesus for him to place his hands on them and pray for them. But the disciples rebuked them. Jesus said, 'Let the little children come to me, and do not hinder them, for the kingdom of heaven belongs to such as these.' When he had placed his hands on them, he went on from there."

- In Mark 10:13-16, it states: "People were bringing little children to Jesus for him to place his hands on them, but the disciples rebuked them. When Jesus saw this, he was indignant. He said to them, 'Let the little children come to me, and do not hinder them, for the kingdom of God belongs to such as these. Truly I tell you, anyone who will not receive the kingdom of God like a little child will never enter it.' And he took the children in his arms, placed his hands on them and blessed them."

- In Luke 18:15-17 we read: "People were also bringing babies to Jesus for him to place his hands on them. When the disciples saw this, they rebuked them. But Jesus called the children to him and said, 'Let the little children come to me, and do not hinder them, for the kingdom of God belongs to such as these. Truly I tell you; anyone who will not receive the kingdom of God like a little child will never enter it.'"

In all three accounts from Matthew 19:13–15; Mark 10:13–16; Luke 18:15–17), it's clear that people (likely parents or guardians) brought their children or infants directly to Christ so He could bless and pray over them.



# Mon  1 Jul 20:54:19 CEST 2024 - in the bible who brought children to christ and he blessed them